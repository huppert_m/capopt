"""
Module contains:
LHCsample(n=1,d=1,d_min=0,d_max=1): latin hyper cube sampling function for a randomized sparse sampling of the
                                    initial training set

CBOopt(object): Constrained Bayesian optimization object, use the following functions:
                add_new_data_point(x, y): Adds configuration x and evaluation of the cost and constraints y to the
                                          Gaussian processes, calculates the posterior and updates the hyperparameters
                get_optimizer_PSO(num_part=100,wmax=0.9,wmin=0.4,c1=1.49445,c2=1.49445,it_min=20,it_max=200):
                                          Calculates the next optimizer of the constrained expected improvement
                                          acquisition function, using particle swarm optimization
                save_data(timestamp, x_crit=np.array([0, 0]), machine_id=116, configuration_id='5', event_id=0, T_i = 3):
                                          Saves all data from self.x (configurations) and self.y (evaluations) into .csv
                                          files for injection into SQL database
"""

import numpy as np
from scipy import stats
import json

def LHCsample(n=1,d=1,d_min=0,d_max=1):
    """Returns a latin hypercube sampling
    n: unsigned integer, number of samples of the LHC
    d: unsigned integer, dimension/number of outputs per sample of the LHC
    d_min: float or array/list of floats with d entries, lower bound of the LHC samples
    d_max: float or array/list of floats with d entries, upper bound of the LHC samples"""

    delta = 1/n
    l = np.linspace(0,1-delta,n)
    Set = np.zeros((n,d))
    r = np.random.rand(n,d)
    for i in range(d):
        lrand = np.random.choice(l,size=(n),replace=False)
        Set[:,i] = lrand+delta*r[:,i]

    Set = Set*(d_max-d_min)+d_min
    return Set

# Bayesian optimization object
class CBOopt(object):
    """
    Parameters
    ----------
    gp: GPy Gaussian processes
    constrlim : float or list of floats
    Safety threshold for the constraint function value. If multiple safety constraints
    are used this can also be a list of floats (the first one is always
    the one for the value, can be set to None if not wanted).
    d_min : Optimization domain lower bound
    d_max: Optimization domain upper bound
    optimization_method: string, optimiztion method of the acquisition function
    stop_eic_abs: Absolute threshold for the constraint expected improvement value of the optimizer
    (Absolute termination criterion)
    stop_eic_rel: Relative threshold of the EIC of the optimizer w.r.t its maximum
    between [0,1) (Relative termination criterion)
    beta: float
    A constant or a function of the time step that scales the confidence
    interval of the acquisition function.
    interval.
    """

    def __init__(self, gp, constrlim, d_min, d_max, optimization_method="PSO", stop_eic_abs=2e-5, stop_eic_rel=0, beta=3):
        if isinstance(gp, list):
            self.gps = gp
        else:
            self.gps = [gp]

        self.constrlim = constrlim
        if self.constrlim[0] is None: # Check if the cost itself is constrained
            self.flag_mainc = 1
        else:
            self.flag_mainc = 0
        self.d_min = d_min # lower bound of domain
        self.d_max = d_max # upper bound of domain
        self.grid = None # list of inputs used for grid evaluation (create using .calculate_grid())
        self.opt_dim = len(d_min) # optimization dimension
        self.beta = beta # parameter for confidence bound calculation self.CB (beta = 2: 95% confidence, beta = 3: 99%)
        self._x = None # optimizition inputs
        self._y = None # observed outputs
        self.best = None # best observation so far
        self.best_idx = 0 # iteration number of self.best
        self.best_arg = None # best configuration
        self.get_initial_xy() # get initial sample(s)
        self.get_best() # get best sample
        self.optimization_method = optimization_method # string, optimization method,
        self.x_opt = None # optimizer
        self.eic_opt = 0 # constrained expected improvement of self.x_opt
        self.eic_max = 0 # maximum eic of any optimizer so far
        self.stop_eic_rel = stop_eic_rel # relative stopping threshold (self.eic_opt < self.stop_eic_rel*self.eic_max)
        self.stop_eic_abs = stop_eic_abs # absolute stopping threshold (self.eic_opt < self.stop_eic_abs)
        # Counter for consecutive iterations, where either the absolute or the relative stopping criterion is fulfilled
        self.stopcount = 0
        self.dim_grid = [0, 0]

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def data(self):
        """
        Return the data within the GP models.
        """
        return self._x, self._y


    def get_initial_xy(self):
        """
        Get the initial x/y data from the GPs.
        """
        self._x = self.gps[0].X
        y = [self.gps[0].Y]

        for gp in self.gps[1:]:
            if np.allclose(self._x, gp.X):
                y.append(gp.Y)
            else:
                raise NotImplemented('The GPs have different measurements.')

        self._y = np.concatenate(y, axis=1)

    def add_data_point(self, gp, x, y):
        """
        Add a data point to a particular GP.

        This should only be called on its own if you know what you're doing.
        This does not update the global data stores self._x and self._y.

        Parameters
        ----------
        x: 2d-array
        y: 2d-array
        gp: instance of GPy.model.GPRegression
            If specified, determines the GP to which we add the data point
            to. Note that this should only be used if that data point is going
            to be removed again.
        """

        gp.set_XY(np.vstack([gp.X, x]),
                  np.vstack([gp.Y, y]))

    def add_new_data_point(self, x, y):
        """
        Add a new function observation to the GPs.

        Parameters
        ----------
        x: 2d-array
        y: 2d-array
        """
        x = np.atleast_2d(x)
        y = np.atleast_2d(y)

        for i, gp in enumerate(self.gps):
            not_nan = ~np.isnan(y[:, i])
            if np.any(not_nan):
                # Add data to GP (context already included in x)
                self.add_data_point(gp, x[not_nan, :], y[not_nan, [i]]) # y: observations of the gps

        # Update global data stores
        self._x = np.concatenate((self._x, x), axis=0)
        self._y = np.concatenate((self._y, y), axis=0)

        # Update best feasible evaluated point
        self.get_best()

    def update_hyperparameters(self):
        """
        Updates the GP hyperparameters
        (Computationally expensive, needs work)
        """
        x_test = LHCsample(10,self.opt_dim,self.d_min,self.d_max)
        x_test = np.atleast_2d(x_test)
        for i in range(len(self.gps)):
            for loop_counter in range(10):
                self.gps[i].optimize_restarts(num_restarts=10, robust = True, verbose=False)
                # Print for observation/bound selection
                #print(self.gps[i].kern.lengthscale)
                #print(self.gps[i].kern.variance)

                # Check if feasible
                mean_x, var_x = self.gps[i].predict(x_test)
                var_x = var_x.squeeze()
                if np.all(var_x) >= 0 and not np.any(np.isnan(var_x)):
                    break
                elif loop_counter >= 9:
                    print("No valid hyperparameters found!")
                else:
                    print("hyperparameters need retuning, setting invalid!")

    def calculate_discretization(self):
        """
        Calculates the discretization of the grid (self.grid) w.r.t. the lengthscales of the GPs
        This should be called after change of GP hyperparameters
        """
        def discretization_resolution(l):
            return np.sqrt(-np.log(0.95)*2*l**2)
        step = discretization_resolution(np.array(self.gps[0].kern.lengthscale))
        for gp_i in self.gps[1:]:
            step = np.vstack((step,discretization_resolution(np.array(gp_i.kern.lengthscale))))
        step = np.atleast_1d(step.min(axis=0))
        step = step[:self.opt_dim]
        step = np.vstack((step,(self.d_max-self.d_min)/3)) # discretizes every direction of x in minimal 3 pieces
        discretization = np.atleast_1d(step.min(axis=0))

        return discretization

    def calculate_grid(self, discretization):
        """
        Calculate grid of possible inputs of the GPs for grid evaluation (get_optimizer_grideval)
        The grid is saved in self.grid as list of inputs

        discretization: numpy array or list of length opt_dim. Discretization step in each dimension of the domain
        """
        # Check if discretization is well defined
        if discretization is None or len(discretization) == 0:
            print("discretization is not defined! "
                  "Discrezitation is set to default (calculate_discretization class function)")
            discretization = self.calculate_discretization()
        elif len(discretization) != self.opt_dim:
            print("dimension of discretization and domain do not coincide. " 
                  "Discrezitation is set to default (calculate_discretization class function)")
            discretization = self.calculate_discretization()
        elif np.any(discretization <= 0):
            print("invalid value used in discretization."
                  "Discrezitation is set to default (calculate_discretization class function)")
            discretization = self.calculate_discretization()

        # Stack and arrange grid list
        discretization_modulo = self.d_max.copy()/discretization.copy() == np.floor(self.d_max.copy()/discretization.copy())
        x = np.arange(self.d_min[0], self.d_max[0]+int(discretization_modulo[0])*discretization[0], discretization[0])
        # [KL] x = np.arange(self.d_min[0], self.d_max[0] + discretization[0], discretization[0])
        for i in range(1,self.opt_dim):
            x_new = np.arange(self.d_min[i], self.d_max[i]+int(discretization_modulo[i])*discretization[i], discretization[i])
            l = np.max(np.shape(x)) # current length of the stack
            l_new = len(x_new) # length of the new dimension
            if i == 1:
                x = np.repeat(x,l_new)
            else:
                x = np.repeat(x,l_new,axis=1)
            x_new = np.tile(x_new,(1,l))
            x = np.vstack((x,x_new))
        self.grid = np.atleast_2d(x).transpose() # take care of 1 dimensional case, grid needs to be min 2 dimensional
        self.dim_grid = [l, l_new]

    def get_best(self):
        """
        Updates best feasible input, output and its index
        """
        idx_feas = np.all(self.y[:,self.flag_mainc:] < self.constrlim[self.flag_mainc:],axis=1)
        feasible_evaluations = self.y[idx_feas==1,:]
        feasible_inputs = self.x[idx_feas==1,:]
        self.best_idx = np.argmin(self.y[idx_feas==1,0])
        self.best = feasible_evaluations[self.best_idx,:]
        self.best_arg = feasible_inputs[self.best_idx,:]

    def get_posterior(self,x,i):
        """
        Returns the posterior of x of the ith gp of self.gps
        """
        mean_x, var_x = self.gps[i].predict(x)
        mean_x = mean_x.squeeze()
        std_dev_x = np.sqrt(var_x.squeeze())

        return mean_x, std_dev_x

    def CB(self,x,i):
        """
        Returns the confidence bounds of x of the ith gp of self.gps
        """
        mean_x, std_dev_x = self.get_posterior(x,i)

        lcb = mean_x - self.beta * std_dev_x
        ucb = mean_x + self.beta * std_dev_x

        return lcb,ucb

    def EIC(self,x):
        """
        Returns constraint expected improvement for x
        """
        # GP posterior prediction
        x = np.atleast_2d(x)
        mean_x = np.zeros((x.shape[0],len(self.gps)),dtype=np.float)
        std_dev_x = np.zeros((x.shape[0],len(self.gps)),dtype=np.float)
        for i in range(len(self.gps)):
            mean_xi, var_xi = self.gps[i].predict_noiseless(x)
            mean_x[:,i] = mean_xi.squeeze()
            std_dev_x[:,i] = np.sqrt(var_xi.squeeze())

        # expected improvement
        u = (self.best[0]-mean_x[:,0])/std_dev_x[:,0]
        ucdf = stats.norm.cdf(u)
        updf = stats.norm.pdf(u)
        ei = std_dev_x[:,0]*(u*ucdf+updf)

        # probability of feasibility
        prfeas = np.ones(x.shape[0],dtype=np.float)
        for i in range(self.flag_mainc,len(self.gps)):
            for k in range(x.shape[0]):
                prfeas[k] = prfeas[k]*stats.norm.cdf(self.constrlim[i],loc=mean_x[k,i],scale=std_dev_x[k,i])

        # constrained expected improvement
        eic = ei*prfeas

        return eic

    def check_termination_criterion(self):
        """
        Handles the stopcount for the absolute and relative termination criterion
        """
        # Update maximum EIC over all iterations
        self.eic_max = np.nanmax((self.eic_opt, self.eic_max))
        # Check stopping criterion and update stopcount
        if self.eic_opt < self.stop_eic_rel*self.eic_max or self.eic_opt < self.stop_eic_abs:
            self.stopcount = self.stopcount+1
        else:
            self.stopcount = 0

    def get_optimizer_PSO(self, num_part=100, wmax=0.9, wmin=0.4, c1=1.49445, c2=1.49445, it_min=20, it_max=200, acquisition_function=None, constraint=lambda x: np.ones((len(x))), safe_set=None):
        """
        Finds the EIC optimizer of self.gps[0] using particle swarm optimization.
        The particles start at random locations p in [dmin, dmax] with velocity v=0.1*p

        acquisition_function: function handle, acquisition function to be optimized
        num_part: Number of particles
        wmax: inertia weight
        wmin: inertia weight
        c1: acceleration factor
        c2: acceleration factor
        it_min: minimal number of iterations
        it_max: maximual number of iterations
        """
        # Default fitness function
        if acquisition_function is None:
            acquisition_function = self.EIC
        # Default safe set
        if safe_set is None:
            safe_set = np.random.rand(num_part,self.opt_dim)*(self.d_max-self.d_min)+self.d_min
        else:
            rand_idx = np.random.randint(0,len(safe_set),num_part)
            safe_set = safe_set[rand_idx]

        tolerance = 1 # stopping criterion of PSO
        it_max = it_max*self.opt_dim # max number of iterations
        fmax = np.zeros(it_max)

        # PSO initialization
        p0 = safe_set
        p = p0.copy()
        v = 0.1*p
        fitness = acquisition_function(p)
        idx_nan = np.isnan(fitness)
        idx_constraint_fulfilled = constraint(p)
        fitness[idx_nan] = 0 # replacing nan entries with 0
        fitness[idx_constraint_fulfilled == 0] = 0 # replacing constraint violating entries with 0
        fitness_max = fitness.max()
        idx_max = fitness.argmax()
        pbest = p.copy()
        gbest = p[idx_max,:].copy()
        it = 0

        while it < it_max and tolerance > 1e-9:
            # velocity update
            w = wmax-(wmax-wmin)*it/it_max
            rand1 = np.random.rand(num_part,self.opt_dim)
            rand2 = np.random.rand(num_part,self.opt_dim)
            v = w*v+c1*rand1*(pbest[:,:self.opt_dim]-p[:,:self.opt_dim])+c2*rand2*(gbest[:self.opt_dim]-p[:,:self.opt_dim])

            # position update
            p[:,:self.opt_dim] = p[:,:self.opt_dim] + v
            # handling boundry violations
            p[:,:self.opt_dim] = np.minimum(p[:,:self.opt_dim],self.d_max)
            p[:,:self.opt_dim] = np.maximum(p[:,:self.opt_dim],self.d_min)

            # evaluating fitness
            fitness_new = acquisition_function(p)
            idx_constraint_fulfilled = constraint(p)

            # fitness update
            for i in range(num_part):
                if fitness_new[i] > fitness[i] and ~np.isnan(fitness_new[i]) and idx_constraint_fulfilled[i]:
                    # check if safe condition is fulfilled
                    pbest[i,:] = p[i,:]
                    fitness[i] = fitness_new[i]

            # update global best
            fitness_max_new = fitness.max()
            fmax[it] = fitness_max_new
            if fitness_max_new > fitness_max and ~np.isnan(fitness_max_new):
                fitness_max = np.copy(fitness_max_new)
                idx_max = fitness.argmax()
                gbest = pbest[idx_max,:].copy()

            if it >= it_min:
                tolerance = np.abs(fmax[it-it_min]-fitness_max)

            it = it+1

        self.x_opt = gbest.copy()
        self.eic_opt = self.EIC(gbest)[0]

        self.check_termination_criterion()

        return self.x_opt.copy(), self.eic_opt.copy()

    def get_optimizer_grideval(self, discretization=None, acquisition_function=None, constraint=lambda x: np.ones((len(x)))):
        """
        Finds the EIC optimizer of self.gps[0] using grid evaluation.

        Parameters
        acquisition_function: function handle, acquisition function to be optimized
        discretization: number or list, array, discretization of the grid, if it does not already exist in self.grid
        """
        # Check for fitness function
        if acquisition_function is None:
            acquisition_function = self.EIC
        # create grid if not defined or empty
        if self.grid is None or len(self.grid) == 0:
            print("grid is not defined, grid is calculated")
            self.calculate_grid(discretization)

        # possible inputs
        input = np.copy(self.grid)
        fitness = acquisition_function(input)

        idx_nan = np.isnan(fitness)
        idx_constraint = constraint(input)
        fitness[idx_nan] = 0  # replacing nan entries with 0
        fitness[idx_constraint == 0] = 0 # replacing constraint violations with 0
        fitness_max = fitness.max()
        idx_max = fitness.argmax()
        gbest = input[idx_max,:].copy()

        self.x_opt = gbest.copy()
        self.eic_opt = self.EIC(self.x_opt)[0]

        self.check_termination_criterion()

        return self.x_opt.copy(), self.eic_opt.copy()

    def get_best_prediction(self, direction=-1, constraint=lambda x: np.ones((len(x))), safe_set=None):
        """
        Finds the optimal mean:
        Use direction +1 to find the maximum predicted mean
        Use direction -1 to find the minimum predicted mean

        Parameters
        direction: integer, use positive value for maximization and negative value for minimization
        """
        optimize_mean = lambda x: np.sign(direction)*self.get_posterior(x,0)[0]
        # "PSO" and "grid evaluation" are implemented
        if self.optimization_method == "PSO":
            x_opt, eic_opt = self.get_optimizer_PSO(acquisition_function=optimize_mean, constraint=constraint, safe_set=safe_set)
        elif self.optimization_method == "grid evaluation":
            x_opt, eic_opt = self.get_optimizer_grideval(acquisition_function=optimize_mean, constraint=constraint)
        else:
            raise NotImplementedError("Undefined optimization method")
        x_opt_mean = np.zeros(len(self.gps))
        x_opt_std = np.zeros(len(self.gps))
        for i in range(len(self.gps)):
            x_opt_mean[i], x_opt_std[i] = self.get_posterior(np.atleast_2d(x_opt),i)

        return x_opt, x_opt_mean, x_opt_std

    def save_data(self, filename="CBOoptimization", timestamp="", source=None, direction=-1, constraint=lambda x: np.ones((len(x))), safe_set=None):
        """
        The samples, their evaluation, the optimum and corresponding configuration as well as the violations and
        corresponding configurations are saved into an .json file into the directory in the source input of form:
        *filename*_*timestamp*.json
        """
        # find invalid inputs
        idx_feas = np.all(self.y[:, self.flag_mainc:] < self.constrlim[self.flag_mainc:], axis=1)
        violations = self.y[idx_feas == 0, :].copy()
        invalid_inputs = self.x[idx_feas == 0, :].copy()

        # find optimal prediction
        x_opt, x_opt_mean, x_opt_std = self.get_best_prediction(direction=direction, constraint=constraint, safe_set=safe_set)

        # build dictionary
        dict_data = {'inputs': self.x.tolist(),
                     'outputs': self.y.tolist(),
                     'optimum_evaluated': self.best_arg.tolist(),
                     'optimum_evaluated cost': self.best.tolist(),
                     'optimum_predicted:': x_opt.tolist(),
                     'optimum_predicted mean:': x_opt_mean.tolist(),
                     'optimum_predicted std:': x_opt_std.tolist(),
                     'violations inputs': invalid_inputs.tolist(),
                     'violations constraint values': violations.tolist()}

        # save data into .csv file
        try:
            jsonString = json.dumps(dict_data)
            if source is None:
                jsonFile = open(filename + "_" + timestamp + ".json", "w")
            else:
                jsonFile = open(source + "/" + filename + "_" + timestamp + ".json", "w")
            jsonFile.write(jsonString)
            jsonFile.close()
        except:
            print("Data could not be saved!")

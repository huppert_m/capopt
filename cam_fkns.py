import ast
import datetime
import os
import time
#from PyQt4 import QtGui
import subprocess

#import h5py
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
from bsread import source, SUB
#from cam_server import PipelineClient
#from cam_server.utils import get_host_port_from_stream_address
from epics import caget
from scipy.interpolate import RectBivariateSpline
from scipy.optimize import fmin


# ---------------------------------------------------------------------------------------------------------------------
# little useful functions you will like
# ---------------------------------------------------------------------------------------------------------------------

# checks if given path exists and creates it if not
def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        print('Creating: ' + directory)
        os.makedirs(directory)


# returns a directory with useful variables containing today's date
# 'path': The path to todays data folder, if no basepath is handed over, the default SF data folder is used
# 'date_sav': date of execution in the format used in file names
# 'date_disp': date of execution in human readable format
def today_vars(basepath='/afs/psi.ch/intranet/SF/data'):
    ret = dict()
    time.strftime("%H-%M-%S")
    ret['path'] = time.strftime(basepath + "/%Y/%m/%d/")
    print('Data folder of today: ' + ret['path'])

    now = datetime.datetime.now()
    ret['date_sav'] = datetime.datetime.strftime(now, "%Y%m%d_%H%M%S")
    ret['date_disp'] = datetime.datetime.strftime(now, "%Y-%m-%d %H:%M:%S")

    return ret


# ---------------------------------------------------------------------------------------------------------------------
# Fit gaussian to 1D data. Automatically find good starting values
# Input: fit_gauss(x values, y values)
# Return: It returns a dictionary with the parameters: amplitude ('amp'), center ('cent'), sigma width ('sig') and
# vertical offset ('off')
# The function 'gaussian' can be used to re-evaluate the fitted data, it takes an x-axis and the prameters.
# The parameters can be either a dictionary as described above or a list of numbers in the above given order.
# ---------------------------------------------------------------------------------------------------------------------

def gaussian(x, params):
    if type(params) is dict:
        val = params['amp'] * np.exp(-(x - params['cent']) ** 2 / (2 * params['sig'] ** 2)) + params['off']
    else:
        val = params[0] * np.exp(-(x - params[1]) ** 2 / (2 * params[2] ** 2)) + params[3]
    return val


def fit_gauss(x, y):
    guess = dict()
    # estimate starting values from the data
    guess['amp'] = np.max(y)
    guess['cent'] = x[np.argmax(y)]
    ynorm = y / np.max(y)
    ym = ynorm[ynorm > 0.5]
    caly = np.abs(x[1] - x[0])
    guess['sig'] = ym.shape[0] * caly / 2.23
    guess['off'] = y.min()

    print('Starting parameters: ' + str(guess))
    params = [guess['amp'], guess['cent'], guess['sig'], guess['off']]

    # define a least squares function to optimize
    def minfunc(params2):
        return sum((y - gaussian(x, params2)) ** 2)

    # fit
    popt = fmin(minfunc, params)

    # Print results
    print("Scale =  %.3f" % (popt[0]))
    print("Center position = %.3f" % (popt[1]))
    print("Sigma =  %.3f" % (popt[2]))
    print("v. offset =  %.3f \n" % (popt[3]))
    return {'amp': popt[0], 'cent': popt[1], 'sig': popt[2], 'off': popt[3]}


# ---------------------------------------------------------------------------------------------------------------------
# Get multiple images from CamServer.
# Input: n: number of images to aquire, camera_name
# !! A screen panel with the used camera has to run with the desired parameters !!
# ---------------------------------------------------------------------------------------------------------------------
def get_multiple_images(camera_name, n):
    instance_name = camera_name + '_sp1'
    error = True
    while error:
        try:
            pipeline_client = PipelineClient()  # connects to default server address
            stream_address = pipeline_client.get_instance_stream(instance_name)
            # Extract the stream hostname and port from the stream address.
            camera_host, camera_port = get_host_port_from_stream_address(stream_address)
            error = False
        except:
            error = True
            # Ask if screen panel should be started
            msgBox = QtGui.QMessageBox()
            msgBox.setIcon(QtGui.QMessageBox.Warning)
            msgBox.setText('The screen panel is not started, do you want to start it now?')
            msgBox.setInformativeText('If you are not sure, press "YES"')
            msgBox.setDetailedText("'Yes': Screen panel will open with the correct camera settings.\n\n" + \
                                   "'No': Please open camera {} on a screen panel.".format(camera_name))
            msgBox.addButton(QtGui.QMessageBox.Yes)
            msgBox.addButton(QtGui.QMessageBox.No)
            msgBox.setDefaultButton(QtGui.QMessageBox.Yes)
            # Comment either line to always answer "no"
            ret = msgBox.exec_()
            #ret = QtGui.QMessageBox.No
            #ret = QtGui.QMessageBox.Yes
            if ret == QtGui.QMessageBox.Yes:
                process = subprocess.Popen(['/sf/op/bin/screen_panel', '-cam=' + camera_name + ' -ct=1'], stdout=subprocess.PIPE)

                while True:
                    output = process.stdout.readline()
                    if process.poll() is not None:
                        break
                    if output:
                        print(output)
                    if output.strip() == b'Camera initialization OK':
                        time.sleep(3)
                        break
                
            else:
                # Ask if screen panel should be started
                msgBox = QtGui.QMessageBox()
                msgBox.setIcon(QtGui.QMessageBox.Warning)
                msgBox.setText('Do you see camera {} on the screen panel?'.format(camera_name))
                msgBox.setInformativeText('If you are not sure, think again')
                msgBox.setDetailedText("'Yes': The program is able to continue.\n\n" + \
                                       "'No': Sorry, you cannot get this image, you will get an exception...".format(camera_name))
                msgBox.addButton(QtGui.QMessageBox.Yes)
                msgBox.addButton(QtGui.QMessageBox.No)
                msgBox.setDefaultButton(QtGui.QMessageBox.Yes)
                # Comment either line to always answer "no"
                ret = msgBox.exec_()
                #ret = QtGui.QMessageBox.No
                #ret = QtGui.QMessageBox.Yes
                if ret == QtGui.QMessageBox.Yes:
                    continue
                else:
                    break
                

            
    image_data = {'x_fit_fwhm':[], 'y_fit_fwhm':[]}

    # Now we subscribe to the streams
    # Opening a stream for the camera data
    with source(host=camera_host, port=camera_port, mode=SUB) as stream:
        for i in range(n):
            message = stream.receive()
            for name, cont in message.data.data.items():
                if name not in image_data:
                    image_data[name] = []
                if name == 'processing_parameters':
                    temp = cont.value
                    temp = temp.replace(': false', ': "False"')
                    temp = temp.replace(': true', ': "True"')
                    temp = temp.replace(': null', ': "None"')
                    image_data[name].append(ast.literal_eval(temp))
                    continue
                # put FWHM of Gauss fit into the data
                if name == 'x_fit_standard_deviation':
                    image_data['x_fit_fwhm'].append(cont.value * 2.35482)
                if name == 'y_fit_standard_deviation':
                    image_data['y_fit_fwhm'].append(cont.value * 2.35482)
                image_data[name].append(cont.value)

    for name, cont in image_data.items():
        if name == 'processing_parameters':
            image_data[name] = image_data[name][0]
            continue
        image_data[name] = np.array(cont)
    


    # add image ROI parameters
    if image_data['processing_parameters']['image_region_of_interest'] != 'None':
        print('ROI on Camserver is set to : ', image_data['processing_parameters']['image_region_of_interest'])
        image_data['width'] = [image_data['processing_parameters']['image_region_of_interest'][1]] * n
        image_data['height'] = [image_data['processing_parameters']['image_region_of_interest'][3]] * n
        image_data['x_roi_start'] = [int(caget(camera_name + ':REGIONX_START')) + image_data['processing_parameters']['image_region_of_interest'][0]] * n
        image_data['y_roi_start'] = [int(caget(camera_name + ':REGIONY_START')) + image_data['processing_parameters']['image_region_of_interest'][2]] * n
        image_data['x_roi_end'] = [image_data['x_roi_start'] + image_data['width']] * n
        image_data['y_roi_end'] = [image_data['y_roi_start'] + image_data['height']] * n
    else:
        image_data['width'] = [int(caget(camera_name + ':WIDTH'))] * n
        image_data['height'] = [int(caget(camera_name + ':HEIGHT'))] * n
        image_data['x_roi_start'] = [int(caget(camera_name + ':REGIONX_START'))] * n
        image_data['y_roi_start'] = [int(caget(camera_name + ':REGIONY_START'))] * n
        image_data['x_roi_end'] = [int(caget(camera_name + ':REGIONX_END'))] * n
        image_data['y_roi_end'] = [int(caget(camera_name + ':REGIONY_END'))] * n





    return image_data


# ---------------------------------------------------------------------------------------------------------------------
# Align multiple images according to their center of masses and sum them. Fit a gaussian function to the profiles of
# the summed image
# Input: stack of images
# ---------------------------------------------------------------------------------------------------------------------
def shift_sum_fit(image_data):
    xaxis_cm = image_data['x_axis'] * 0
    yaxis_cm = image_data['y_axis'] * 0
    imgs_model = []
    n = xaxis_cm.shape[0]

    # build models for each image
    # the axes are iverted because of the negative convention of SF cameras, the interpolation expects increasing values
    for i in range(n):
        xaxis_cm[i] = -(image_data['x_axis'][i] - image_data['x_center_of_mass'][i])
        yaxis_cm[i] = -(image_data['y_axis'][i] - image_data['y_center_of_mass'][i])
        imgs_model.append(RectBivariateSpline(yaxis_cm[i], xaxis_cm[i], image_data['image'][i, :, :]))

    # find new common grid
    dx = image_data['x_axis'][0, 0] - image_data['x_axis'][0, 1]
    dy = image_data['y_axis'][0, 0] - image_data['y_axis'][0, 1]
    x_comm = np.arange(np.max(xaxis_cm[:, 0]), np.min(xaxis_cm[:, -1]) + dx, dx)
    y_comm = np.arange(np.max(yaxis_cm[:, 0]), np.min(yaxis_cm[:, -1]) + dy, dy)

    imgs_intp = np.zeros((n, y_comm.size, x_comm.size))
    # interpolate onto new grid
    for i in range(n):
        # print(i)
        imgs_intp[i, :, :] = imgs_model[i](y_comm, x_comm)

    x_plot = x_comm - np.mean(image_data['x_center_of_mass'])
    y_plot = y_comm - np.mean(image_data['y_center_of_mass'])

    img_sum = np.sum(imgs_intp, 0)
    x_prof_sum = np.sum(img_sum, 0)
    y_prof_sum = np.sum(img_sum, 1)

    print('Fit x integration')
    xfitresult = fit_gauss(x_plot, x_prof_sum)
    print('Fit y integration')
    yfitresult = fit_gauss(y_plot, y_prof_sum)

    # modify data structure for summed image
    img_new = {'image': [img_sum], 'x_axis': [x_plot], 'y_axis': [y_plot], \
               'x_profile': [x_prof_sum], 'y_profile': [y_prof_sum],
               'x_fit_gauss_function': [gaussian(x_plot, xfitresult)], \
               'y_fit_gauss_function': [gaussian(y_plot, yfitresult)], \
               'y_fit_amplitude': [yfitresult['amp']], 'y_fit_mean': [yfitresult['cent']],
               'y_fit_offset': [yfitresult['off']], 'y_fit_standard_deviation': [yfitresult['sig']], \
               'x_fit_amplitude': [xfitresult['amp']], 'x_fit_mean': [xfitresult['cent']],
               'x_fit_offset': [xfitresult['off']], 'x_fit_standard_deviation': [xfitresult['sig']], \
               'height': [y_plot.size], 'width': [x_plot.size], 'intensity': [np.sum(img_sum)],
               'max_value': [np.max(img_sum)], \
               'min_value': [np.min(img_sum)]}
    img_return = {**image_data, **img_new}
    return img_return


# ---------------------------------------------------------------------------------------------------------------------
# Saves an image or a stack of images into the standardized SF camara data format (.h5)
# Input: imgage data as python dictionary e.g. output from shift_sum_fit
# ---------------------------------------------------------------------------------------------------------------------

def save_img(image_data, path=''):
    if path == '':
        tod = today_vars()
        ensure_dir(tod['path'])
        path = tod['path'] + tod['date_sav'] + '_' + image_data['processing_parameters']['camera_name'] + '.h5'
    hf = h5py.File(path, 'w')
    hf.attrs[u'file_name'] = path
    hf.attrs[u'Camera'] = image_data['processing_parameters']['camera_name']
    mydata = hf.create_group(u'Camera1')

    for name, cont in image_data.items():
        if name == 'processing_parameters':
            myparams = hf.create_group(u'processing_parameters')
            for pname, pcont in image_data['processing_parameters'].items():
                myparams.create_dataset(pname, data=pcont)
            continue
        mydata.create_dataset(name, data=cont)
    hf.close()


# ---------------------------------------------------------------------------------------------------------------------
# Reads an image or a stack of images from the standardized SF camara data format (.h5) into a python directory
# Input: full path to .h5 file camera data
# ---------------------------------------------------------------------------------------------------------------------

def read_h5_img(file_path):
    image_data = dict()
    with h5py.File(file_path, 'r') as h5file:
        for name in h5file:
            if name == 'Camera1':
                for subname in h5file['Camera1']:
                    image_data[subname] = h5file['Camera1/' + subname].value
            else:
                image_data[name] = dict()
                for subname in h5file[name]:
                    image_data[name][subname] = h5file[name + '/' + subname].value
    return image_data


# ---------------------------------------------------------------------------------------------------------------------
# gets one or multiple images from a laser camera via epics and returns them as dictionary, compatiple to cam_server
# !!! the image data and the fitted values are not exactly synchronized !!!
# The results from the fit come from Edwin's expert panel and are subject to background substraction and 
# ROI as defined there. The image data is raw data.
# ---------------------------------------------------------------------------------------------------------------------

def get_multiple_lcam_images(camera_name, n):
    image_data = dict()
    keys = ['y_default', 'y_calibration', 'processing_parameters', 'x_default', 'x_fit_fwhm', 'y_fit_fwhm', 'width', \
            'min_value', 'y_axis', 'x_fit_mean', 'x_fit_standard_deviation', 'y_fit_mean', 'y_profile', 'max_value', \
            'x_axis', 'image', 'y_fit_standard_deviation', 'x_calibration', 'height', 'x_profile', 'intensity',
            'timestamp', 'x_roi_start', 'x_roi_end', 'y_roi_start', 'y_roi_end']
    for key in keys:
        image_data[key] = []
    w = int(caget(camera_name + ':WIDTH'))
    h = int(caget(camera_name + ':HEIGHT'))
    image_data['processing_parameters'] = {'camera_name': camera_name, 'description': caget(camera_name + ':INIT.DESC'),
                                           'number_images': n}

    for i in range(0, n):
        image_data['width'].append(w)
        image_data['height'].append(h)
        image_data['x_default'].append(caget(camera_name + ':DEFAULT-X'))
        image_data['y_default'].append(caget(camera_name + ':DEFAULT-Y'))
        image_data['x_calibration'].append(caget(camera_name + ':PIXELSIZE'))
        image_data['y_calibration'].append(caget(camera_name + ':PIXELSIZE'))

        temp = caget(camera_name + ':FPICTURE')
        #image_data['timestamp'].append(caget(camera_name + ':CAPTURE_OK'))
        #image_data['x_fit_mean'].append(caget(camera_name + ':FIT-XPOS_EGU'))
        #image_data['y_fit_mean'].append(caget(camera_name + ':FIT-YPOS_EGU'))
        #image_data['x_fit_standard_deviation'].append(caget(camera_name + ':FIT-XWID_EGU'))
        #image_data['y_fit_standard_deviation'].append(caget(camera_name + ':FIT-YWID_EGU'))

        temp = np.squeeze(temp[0:(w * h)])
        image_data['image'].append(np.reshape(temp, (h, w)))
        image_data['x_axis'].append(
            (np.arange(0, image_data['width'][-1]) - int(image_data['x_default'][-1])) * image_data['x_calibration'][-1])
        image_data['x_profile'].append(np.sum(image_data['image'][-1], 0))
        image_data['y_axis'].append(
            (np.arange(0, image_data['height'][-1]) - int(image_data['y_default'][-1])) * image_data['y_calibration'][-1])
        #image_data['x_fit_fwhm'].append(image_data['x_fit_standard_deviation'][-1] * 2.355)
        #image_data['y_fit_fwhm'].append(image_data['y_fit_standard_deviation'][-1] * 2.355)
        image_data['y_profile'].append(np.sum(image_data['image'][-1], 1))
        image_data['max_value'].append(np.max(image_data['image'][-1]))
        image_data['min_value'].append(np.min(image_data['image'][-1]))
        image_data['intensity'].append(np.sum(image_data['image'][-1]))

    for name, cont in image_data.items():
        if name == 'processing_parameters':
            continue
        image_data[name] = np.array(cont)

    return image_data


# ---------------------------------------------------------------------------------------------------------------------
# returns the mean of an image stack on all parameters
# ---------------------------------------------------------------------------------------------------------------------

def mean_image(image_data):
    for name, cont in image_data.items():
        if name == 'processing_parameters':
            continue
        image_data[name] = np.array([np.mean(cont, axis=0)])
    return image_data


# ---------------------------------------------------------------------------------------------------------------------
# returns the difference of two images in all parameters !!!! function not used anymore !!!!
# ---------------------------------------------------------------------------------------------------------------------

def image_difference(image_data, image_data_ref):
    image_data_diff = {'warn': [], 'err': []}
    change_warn = ['x_calibration', 'y_calibration', 'x_default', 'y_default', 'x_axis', 'y_axis']
    change_err = ['height', 'width']
    difflist = ['x_fit_standard_deviation', 'y_fit_standard_deviation', 'y_fit_mean', 'x_fit_fwhm', 'x_profile',
                'y_profile', \
                'max_value', 'y_fit_fwhm', 'image', 'min_value', 'x_fit_mean', 'intensity', 'x_calibration',
                'y_calibration', 'x_default', 'y_default']

    for key, cont in image_data.items():
        if key in change_err:
            if (image_data[key] == image_data_ref[key]).all():
                image_data_diff[key] = image_data[key]
            else:
                image_data_diff['err'].append(
                    key.replace('_', '-') + ' ' + '%.2f' % image_data_ref[key][0] + ' to ' + '%.2f' % image_data[key][
                        0])
    if not image_data_diff['err'] == []:
        return image_data_diff

    for key, cont in image_data.items():
        if key in change_warn:
            if (image_data[key] == image_data_ref[key]).all():
                image_data_diff[key] = image_data[key]
            else:
                if image_data[key].size == 1:
                    image_data_diff['warn'].append(
                        key.replace('_', '-') + ' ' + '%.2f' % image_data_ref[key][0] + ' to ' + '%.2f' %
                        image_data[key][0])
                else:
                    image_data_diff['warn'].append(key.replace('_', '-') + ' changed')

                if key == 'x_axis':
                    image_data_diff[key] = image_data[key]
                    for i in range(0, image_data['x_axis'].shape[0]):
                        image_data_diff[key][i, :] = np.arange(0, image_data['width'])
                elif key == 'y_axis':
                    image_data_diff[key] = image_data[key]
                    for i in range(0, image_data['y_axis'].shape[0]):
                        image_data_diff[key][i, :] = np.arange(0, image_data['height'])
                else:
                    image_data_diff[key] = np.nan
        if key in difflist:
            image_data_diff[key] = image_data[key] - image_data_ref[key]
        else:
            image_data_diff[key] = image_data[key]

    changes = dict()
    comp_self = ['x_calibration', 'y_calibration', 'x_fit_standard_deviation', 'y_fit_standard_deviation', 'x_fit_fwhm', \
                 'y_fit_fwhm', 'max_value', 'min_value', 'intensity']
    comp_cross = {'x_fit_mean': 'x_fit_fwhm', 'y_fit_mean': 'y_fit_fwhm'}

    for key, cont in image_data_diff.items():
        if key in comp_cross:
            if image_data[comp_cross[key]][0] != 0:
                changes[key + '_per'] = float(cont[0]) / float(image_data[comp_cross[key]][0]) * 100
        elif key in comp_self:
            if image_data[key][0] != 0:
                changes[key + '_per'] = float(cont[0]) / float(image_data[key][0]) * 100
    changes['max_change'] = np.max(np.abs(np.fromiter(iter(changes.values()), dtype=float)))

    return changes, image_data_diff


# ---------------------------------------------------------------------------------------------------------------------
# plots image data and writes a png into the path given in image_data['processing_parameters']['pngpath']
# ---------------------------------------------------------------------------------------------------------------------
def camplot(self, image_data, cmap='jet'):
    if not cmap:
        cmap = self.opts_loc['cmap']
    fig = plt.figure(figsize=(8, 12))
    fig.clf()
    ax = plt.subplot(211, aspect='equal', adjustable='box-forced')
    if 'colorlimit' in self.opts_loc.keys(): 
        im = ax.pcolormesh((image_data['x_axis'][0]), image_data['y_axis'][0], image_data['image'][0], rasterized=True,
                       cmap=cmap, vmax=self.opts_loc['colorlimit'])
    else:
        im = ax.pcolormesh((image_data['x_axis'][0]), image_data['y_axis'][0], image_data['image'][0], rasterized=True,
                       cmap=cmap)
    divider = make_axes_locatable(plt.gca())
    cax = divider.append_axes("right", "5%", pad="3%")
    plt.colorbar(im, cax=cax)

    #if (('warn', []) in image_data.items()) | (not ('warn' in image_data)):
    ax.axhline(0, color='r')
    ax.axvline(0, color='r')
    ax.axhline(image_data['y_fit_mean'], color='g')
    ax.axvline(image_data['x_fit_mean'], color='g')
    ax.set_xlim([np.min(image_data['x_axis']), np.max(image_data['x_axis'])])
    ax.set_ylim([image_data['y_axis'][-1][0], image_data['y_axis'][-1][-1]])
    # region of interest
    if 'roiwidth' in self.opts_loc.keys():
        ax.set_xlim([-self.opts_loc['roiwidth']/2, self.opts_loc['roiwidth']/2])
    if 'roiheight' in self.opts_loc.keys():
        ax.set_ylim([-self.opts_loc['roiheight']/2, self.opts_loc['roiheight']/2])
    ax.set_xlabel('camera position / um')
    ax.set_ylabel('camera position / um')
    ax.invert_yaxis()
    '''if 'warn' in image_data:
        if image_data['warn']:
            ax.set_xlim([np.min(image_data['x_axis']), np.max(image_data['x_axis'])])
            ax.set_ylim([np.min(image_data['y_axis']), np.max(image_data['y_axis'])])
            ax.set_xlabel('camera pixel')
            ax.set_ylabel('camera pixel')'''
    if self.opts_loc['crosssec']['enabled']:
        ax2 = plt.subplot(212)
        if self.opts_loc['crosssec']['type']=='projection':
            xline = ax2.plot(image_data['x_axis'][0], image_data['x_profile'][0], label='x-profile')
            yline = ax2.plot(image_data['y_axis'][0], image_data['y_profile'][0], label='y-profile')
            ax2.set_title('Pojected intensity', color='k', fontsize=15)
        if self.opts_loc['crosssec']['type']=='cut':
            i = self.opts_loc['crosssec']['cutwidth']
            xprofile = np.sum(image_data['image'][0][int(np.argwhere(image_data['y_axis'][0] == 0))-i:int(np.argwhere(image_data['y_axis'][0] == 0))+i,:], 0)
            yprofile = np.sum(image_data['image'][0][:, int(np.argwhere(image_data['x_axis'][0] == 0))-i:int(np.argwhere(image_data['x_axis'][0] == 0))+i], 1)
            xprofnorm = xprofile/np.max(xprofile)
            yprofnorm = yprofile/np.max(yprofile)
            xm = xprofnorm[xprofnorm>0.5]
            ym = yprofnorm[yprofnorm>0.5]
            fwhmx = xm.shape[0]*image_data['x_calibration'][0]
            fwhmy = ym.shape[0]*image_data['y_calibration'][0]

            xline = ax2.plot(image_data['x_axis'][0], xprofile, label='X, FWHM/um: ' + str(np.round(fwhmx,1)))
            yline = ax2.plot(image_data['y_axis'][0], yprofile, label='Y, FWHM/um: ' + str(np.round(fwhmy,1)))
            ax2.set_title('Cut through center +/- ' + str(i) + ' pixels', color='k', fontsize=15)

        ax2.set_xlabel('Camera position / um')
        ax2.set_ylabel('Counts')
        ax2.legend(bbox_to_anchor=(1.,1), frameon=False)
        asp = np.diff(ax2.get_xlim())[0] / np.diff(ax2.get_ylim())[0]
        ax2.set_aspect(asp/3)

    ax.set_aspect(1)
    #plt.tight_layout()
    if self.opts_loc['save']:
        fig.savefig(self.opts_loc['pngpath'], dpi=150, bbox_inches='tight')
    if self.opts_loc['show']:
        plt.show()

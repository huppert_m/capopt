env = 'mhcs'

import subprocess
envs = subprocess.check_output(['conda', 'env', 'list'])
if env in str(envs):
	print('Conda environment {} exists, remove it...'.format(env))
	subprocess.run(['conda', 'env', 'remove', '-n', env, '--all'])
else:
	print('Conda environment {} does not exist. Nothing done'.format(env))
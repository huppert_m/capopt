env = 'capopt'

import subprocess
envs = subprocess.check_output(['conda', 'env', 'list'])
if env in str(envs):
	print('Conda environment {} already exists, update it'.format(env))
	subprocess.run(['conda', 'env', 'update', '-f', env +'.yml'])
else:
	print('Conda environment {} does not exist. Create it...'.format(env))
	subprocess.run(['conda', 'env', 'create', '-f', env + '.yml'])
